#!/bin/bash

# Teams incoming web-hook URL 
url=''                
curlheader='-H "Content-Type: application/json"'
agent='-A "ZabbixAlertScript"'
curlmaxtime='-m 60'

subject="$2"
message="$3"

# Change message themeColor depending on the subject - green (Resolved), red (Problem), or grey (for everything else)

recoversub='^Resolved?'
problemsub='^Problem?'
if [[ "$subject" =~ ${recoversub} ]]; then
        THEMECOLOR='00fa19'
elif [[ "$subject" =~ ${problemsub} ]]; then
        THEMECOLOR='fa0000'
else
        THEMECOLOR='7a7777'
fi

# The message that we want to send to Slack is the "subject" value ($2 / $subject - that we got earlier)
#  followed by the message that Zabbix actually sent us ($3)


# Build our JSON payload and send it as a POST request to the Slack incoming web-hook URL

payload=\""{
                \\\"@type\\\": \\\"MessageCard\\\",
        \\\"@context\\\": \\\"http://schema.org/extensions\\\",
                \\\"title\\\": \\\"${subject} \\\",
                \\\"text\\\": \\\"${message} \\\",
        \\\"summary\\\": \\\"${subject} \\\",
                \\\"themeColor\\\": \\\"${THEMECOLOR}\\\",
                \\\"potentialAction\\\": [
                                        {
                                        \\\"@type\\\": \\\"OpenUri\\\",
                                        \\\"name\\\": \\\"ACN Zabbix\\\",
                                        \\\"targets\\\": [
                                                {\\\"os\\\": \\\"default\\\", \\\"uri\\\": \\\"Your URL To Zabbix\\\" }
                                                ]
                                        }
                                ]
        }"\"

curldata=$(echo -d "$payload")
eval curl $curlmaxtime $curlheader $curldata $url $agent