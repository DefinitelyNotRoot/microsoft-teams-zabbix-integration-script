
**Microsoft-Teams-Zabbix-Integration-Script**

**Deploy**

1. Add this script to alert scripts:
   1.  cat /etc/zabbix/zabbix_server.conf | grep -i alert -> AlertScriptsPath=/usr/lib/zabbix/alertscripts
   2. Add Connector to Teams like here: https://sankalpit.com/how-to-get-channel-webhook-url/
   3. Edit script add your url to webhook and url to zabbix
   4. Finish

**This is how it is gonna looks like on Microsoft Teams:**

![](screenshot.png)


Inspirations:
https://github.com/spasticate/zabbix-MSTeams-alertscript
https://github.com/theguimaraes/zabbix/tree/master/MSTeams-AlertScript